<p align="center">
  <h1 align="center"> MassJ.jl  </h1>
</p>
<p align="center">
  <img align="center" src="docs/src/assets/logo.png" width="400" height="200" />
</p>
<p align="center">
  <normal> A mass spectrometry package for Julia </normal>
</p>



##


## Installation
This package is unregistered. It can be installed either with the Julia package manager.
From the Julia REPL, type `]` to enter the Pkg REPL mode and run:
```julia
pkg> add https://gitlab.com/massj/massj.jl
```
or using the package API:

```julia
using Pkg
Pkg.add(PackageSpec(url="https://gitlab.com/massj/massj.jl"))
```


## Usage
MassJ is a package for loading, processing and plotting mass spectrometry data. It provides the following functionalities:

    Getting information on the file
    Load a file
    Averaging mass spectra based on various criteria that may be combined
    Chromatogram and extracted chromatograms
    Processing the data
        smoothing
        baseline correction
        peak-picking
    Calculation of isotopic distribution

To get information on a file:
```julia
info("path/to/file")
```

Mass spectra can be loaded by:
```julia
data = load("path/to/file")
```

And averaged as follow:
```julia
ms1 = average(data, Level(1))                   # full MS scans
ms2 = average(data, Level(2))                   # MS2 spectra
ms3 = average(data, Activation_Method("CID"))   # CID spectra
```

## Supported file format
* mzxml

## Other Julia packages
* [mzXML.jl](https://github.com/timholy/mzXML.jl): Load mass spectrometry mzXML files.
* [MassSpec.jl](https://github.com/JuliaSpect/MassSpec.jl): Mass spectometry utilities for Julia
